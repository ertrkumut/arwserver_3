package main

import (
	"fmt"
	"net"
)

type SessionManager struct {
	allSessions      []*Session
	sessionIdCounter uint32
}

func (sessionManager *SessionManager) StartSession(conn net.Conn) *Session {
	var session *Session
	session = new(Session)

	session.Init(conn, sessionManager)
	return session
}

func (sessionManager *SessionManager) CloseSession(arwServer *ARWServer, conn net.Conn) {

	user, err := arwServer.userManager.FindUserWithSession(conn)

	if err == nil {
		user.DestroyUser(arwServer)
	}

	for ii, ses := range sessionManager.allSessions {
		if ses.conn == conn {
			fmt.Println("Session Close : ", ses.conn.RemoteAddr())
			arwServer.logManager.NewSystemLog("Session Close : "+ses.conn.RemoteAddr().String(), System_Log)
			sessionManager.allSessions = append(sessionManager.allSessions[:ii], sessionManager.allSessions[ii+1:]...)
			ses.udpAddr = nil
			ses.conn.Close()
			ses.conn = nil
			ses = nil
			return
		}
	}
	conn.Close()
	conn = nil
}

func (sessionManager *SessionManager) FindSessionIdByID(session_id uint32) *Session {

	for _, ses := range sessionManager.allSessions {
		if ses.id == session_id {
			return ses
		}
	}

	return nil
}

func (sessionManager *SessionManager) SessionIsExitsByVoicePort(voicePort *net.UDPAddr) bool {
	for _, session := range sessionManager.allSessions {
		if session.voiceaddr != nil && session.voiceaddr.String() == voicePort.String() {
			return true
		}
	}
	return false
}
