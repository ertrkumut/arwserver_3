package main

import (
	"net"
)

type RequestManager struct {
	wrongTCPData string
	beginTCPReq  bool
	wrongUDPData string
	beginUDPReq  bool
}

func (reqManager *RequestManager) StartOrStopRequest(arwServer *ARWServer, conn net.Conn) {
	if reqManager.beginTCPReq == false {
		reqManager.beginTCPReq = true
		return
	}

	request := ExtractRequest([]byte(reqManager.wrongTCPData))
	reqManager.DoTcpRequest(arwServer, request, conn)

	reqManager.wrongTCPData = ""
	reqManager.beginTCPReq = false
}

func (reqManager *RequestManager) AddTcpChar(reqChar string) {
	reqManager.wrongTCPData += reqChar
}

func (reqManager *RequestManager) DoTcpRequest(arwServer *ARWServer, request *Request, conn net.Conn) {
	for ii := 0; ii < len(arwServer.events.allEvents); ii++ {
		currentEvent := arwServer.events.allEvents[ii]

		if currentEvent.eventName == request.eventname {
			currentEvent.Private_Handler(arwServer, conn, request)
			request = nil
			return
		}
	}
	request = nil
}

//============================================

func (reqManager *RequestManager) StartOrStopUDPRequest(arwServer *ARWServer, conn *net.UDPAddr) {
	if reqManager.beginUDPReq == false {
		reqManager.beginUDPReq = true
		return
	}

	request := ExtractRequest([]byte(reqManager.wrongUDPData))
	reqManager.DoUDPRequest(arwServer, request, conn)
	reqManager.wrongUDPData = ""
	reqManager.beginUDPReq = false
}

func (reqManager *RequestManager) AddUdpChar(reqChar string) {
	reqManager.wrongUDPData += reqChar
}

func (reqManager *RequestManager) DoUDPRequest(arwServer *ARWServer, request *Request, conn *net.UDPAddr) {
	for ii := 0; ii < len(arwServer.events.allEvents); ii++ {
		currentEvent := arwServer.events.allEvents[ii]

		if currentEvent.eventName == request.eventname {
			currentEvent.Private_Handler_UDP(arwServer, conn, request)
			return
		}
	}
}

//============================================
