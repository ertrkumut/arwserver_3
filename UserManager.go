package main

import (
	"errors"
	"fmt"
	"net"
)

type UserManager struct {
	allUser       []*ARWUser
	userIdCounter int
}

func (userManager *UserManager) CreateUser(arwServer *ARWServer, conn net.Conn, userName string) (*ARWUser, error) {

	for ii := 0; ii < len(arwServer.sessionManager.allSessions); ii++ {
		if arwServer.sessionManager.allSessions[ii].conn != nil && arwServer.sessionManager.allSessions[ii].conn == conn {
			newUser := CreateUser(userName, userManager.userIdCounter, arwServer.sessionManager.allSessions[ii])
			userManager.userIdCounter++
			userManager.allUser = append(userManager.allUser, newUser)
			return newUser, nil
		}
	}

	return nil, errors.New("Session Not Found!")
}

func (userManager *UserManager) CreateBotUser() *ARWUser {
	newUser := new(ARWUser)
	newUser.name = fmt.Sprintf("Bot_%v", userManager.userIdCounter)
	newUser.id = userManager.userIdCounter
	newUser.isBot = true
	userManager.userIdCounter++
	userManager.allUser = append(userManager.allUser, newUser)

	return newUser
}

func (userManager *UserManager) Removeuser(arwServer *ARWServer, user *ARWUser) {
	for ii := 0; ii < len(userManager.allUser); ii++ {
		if userManager.allUser[ii] == user {
			userManager.allUser = append(userManager.allUser[:ii], userManager.allUser[ii+1:]...)
		}
	}
}

func (userManager *UserManager) FindUserWithId(userId int) (*ARWUser, error) {

	for ii := 0; ii < len(userManager.allUser); ii++ {
		if userManager.allUser[ii].id == userId {
			return userManager.allUser[ii], nil
		}
	}

	arwServer.logManager.NewSystemLog("Find User With ID Error : User does not exist!", System_Error)
	return nil, errors.New("User Does not Exist")
}

func (userManager *UserManager) FindUserWithSession(conn net.Conn) (*ARWUser, error) {

	for ii := 0; ii < len(userManager.allUser); ii++ {
		if userManager.allUser[ii].session.conn == conn {
			return userManager.allUser[ii], nil
		}
	}

	arwServer.logManager.NewSystemLog("Find User With Session Error : User does not exist!", System_Error)
	return nil, errors.New("User Does not Exist")
}

func (userManager *UserManager) FindUserWithSessionId(session uint32) (*ARWUser, error) {

	for ii := 0; ii < len(userManager.allUser); ii++ {
		if userManager.allUser[ii].session.id == session {
			return userManager.allUser[ii], nil
		}
	}

	arwServer.logManager.NewSystemLog("Find User With Session ID Error : User does not exist!", System_Error)
	return nil, errors.New("User Does not Exist")
}

func (userManager *UserManager) FindUserWithUDPSession(conn *net.UDPAddr) (*ARWUser, error) {

	for ii := 0; ii < len(userManager.allUser); ii++ {
		if userManager.allUser[ii].session.udpAddr.String() == conn.String() {
			return userManager.allUser[ii], nil
		}
	}

	arwServer.logManager.NewSystemLog("Find User With UDP ID Error : User does not exist!", System_Error)
	return nil, errors.New("User Does not Exist")
}
