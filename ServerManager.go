package main

import (
	"fmt"
)

var arwServer *ARWServer

func main() {
	arwServer = new(ARWServer)

	arwServer.AddEventHandler(&(arwServer.events.Login), OnLoginEventHandler)
	arwServer.AddExtensionHandler("Collision", CollisionHandler)

	arwServer.Initialize()
	arwServer.InitUDP()
	arwServer.voiceManager.Init(10)

	arwServer.ProcessEvents()
}

func OnLoginEventHandler(arwObj ARWObject) {
	user, err := arwObj.GetUser(arwServer)
	if err != nil {
		fmt.Println("Login Event Find User Error")
		return
	}

	roomSettings := new(RoomSettings)
	roomSettings.cappacity = 10
	roomSettings.name = "Test"
	roomSettings.tag = "Test"
	room := arwServer.roomManager.CreateRoom(arwServer, roomSettings)
	if room == nil {
		return
	}

	room.AddExtensionHandler("Extension_Test", Extension_TestHandler)
	room.AddUserInRoom(user, arwServer)
}

func CollisionHandler(arwServer *ARWServer, user *ARWUser, arwObj ARWObject) {
	hello, _ := arwObj.GetString("Hello")
	fmt.Println(hello)
}
func Extension_TestHandler(arwServer *ARWServer, user *ARWUser, arwObj ARWObject, room *ARWRoom) {
	hello, _ := arwObj.GetString("Hello")
	fmt.Println(hello)
	arwServer.SendExtensionRequest("UDP_Test", user, arwObj, false)
}

func Find_FreeForAll(arwServer *ARWServer, user *ARWUser, arwObj ARWObject) {
}
