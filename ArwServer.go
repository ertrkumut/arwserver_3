package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"net/http"
)

type ARWServer struct {
	serverSettings    ServerSettings
	events            ARWEvents
	tcpListener       net.Listener
	udpListener       *net.UDPConn
	sessionManager    SessionManager
	requestManager    RequestManager
	voiceManager      ARWVoiceChatManager
	userManager       UserManager
	roomManager       RoomManager
	logManager        LogManager
	extensionHandlers []*ExtensionRequest
}

const (
	System_Log   = "SYSTEM_LOG"
	System_Error = "SYSTEM_ERROR"
)

func (arwServer *ARWServer) Initialize() {
	arwServer.serverSettings.InitializeServerSettings("ServerFiles/ServerProperties.json")
	arwServer.events.Initialize()

	arwServer.logManager.Init("ServerFiles/Log.txt")
	serverAddr, err := net.ResolveTCPAddr("tcp", arwServer.serverSettings.tcpPort)
	if err != nil {
		panic(err)
	}
	tempListener, listenerError := net.ListenTCP("tcp", serverAddr)

	if listenerError != nil {
		panic(listenerError)
	}
	arwServer.tcpListener = tempListener
	fmt.Println("ArwServer Initialize Success \n\n")
	arwServer.logManager.NewSystemLog("ARWServer Intiailize Success", System_Log)
}

func (arwServer *ARWServer) InitUDP() {
	arwServer.requestManager.beginUDPReq = false
	serverAddr, err := net.ResolveUDPAddr("udp", arwServer.serverSettings.udpPort)
	if err != nil {
		fmt.Println("UDP Init Error 1 ", err)
		arwServer.logManager.NewSystemLog("UDP Initialize Error : "+err.Error(), System_Error)
		return
	}
	udpListener, err := net.ListenUDP("udp", serverAddr)

	if err != nil {
		fmt.Println("UDP Init Error 2 ", err)
		arwServer.logManager.NewSystemLog("UDP Initialize Error : "+err.Error(), System_Error)
		return
	}
	arwServer.udpListener = udpListener
	fmt.Println("UDP Init Success...\n")
	arwServer.logManager.NewSystemLog("UDP Initialize Success", System_Log)
}

func (arwServer *ARWServer) ProcessEvents() {
	defer arwServer.tcpListener.Close()
	if arwServer.udpListener != nil {
		defer arwServer.udpListener.Close()
	}

	// go func() {
	// 	for {
	// 		var m runtime.MemStats
	// 		runtime.ReadMemStats(&m)
	// 		fmt.Printf("Alloc = %v MiB", bToMb(m.Alloc))
	// 		fmt.Printf("\tTotalAlloc = %v MiB", bToMb(m.TotalAlloc))
	// 		fmt.Printf("\tSys = %v MiB", bToMb(m.Sys))
	// 		fmt.Printf("\tNumGC = %v\n", m.NumGC)
	// 		time.Sleep(2 * time.Second)
	// 	}
	// }()
	go func() {
		for {
			arwServer.UDPHandler()
		}
	}()
	go func() {
		for {
			arwServer.voiceManager.ProcessEvent()
		}
	}()
	for {
		arwServer.TCPHandler()
	}
}

func (arwServer *ARWServer) TCPHandler() {
	conn, acceptErr := arwServer.tcpListener.Accept()

	if acceptErr != nil {
		fmt.Println("Error Accepting :", acceptErr)
		arwServer.logManager.NewSystemLog("TCP Packet Accepting Error : "+acceptErr.Error(), System_Error)
	} else {
		go arwServer.HandleRequests(conn)
	}

	acceptErr = nil
}

func (arwServer *ARWServer) UDPHandler() {
	if arwServer.udpListener == nil {
		return
	}

	buf := make([]byte, 1024)
	_, udpAddr, err := arwServer.udpListener.ReadFromUDP(buf)
	if err != nil {
		fmt.Println("Read UDP error ", err)
		arwServer.logManager.NewSystemLog("Read UDP Error : "+err.Error(), System_Error)
	} else {
		buf = bytes.Trim(buf, "\x00")
		go arwServer.ParseUDPRequestBytes(buf, udpAddr)
	}
}

func (arwServer *ARWServer) HandleRequests(conn net.Conn) {
	defer conn.Close()
	for {
		requestBytes := make([]byte, 1024)

		_, err := conn.Read(requestBytes)

		if err != nil {
			if err == io.EOF {
				arwServer.sessionManager.CloseSession(arwServer, conn)
				requestBytes = nil
				err = nil
				return
			}
			requestBytes = nil
			err = nil
			arwServer.sessionManager.CloseSession(arwServer, conn)
			return
		}

		requestBytes = bytes.Trim(requestBytes, "\x00")
		arwServer.ParseRequestBytes(requestBytes, conn)
		requestBytes = nil
		err = nil
	}
}

func (arwServer *ARWServer) ParseRequestBytes(bytes []byte, conn net.Conn) {
	message := string(bytes)

	for ii := 0; ii < len(message); ii++ {
		if message[ii] == '|' {
			arwServer.requestManager.StartOrStopRequest(arwServer, conn)
		} else {
			arwServer.requestManager.AddTcpChar(string(message[ii]))
		}
	}
}

func (arwServer *ARWServer) ParseUDPRequestBytes(bytes []byte, conn *net.UDPAddr) {
	message := string(bytes)
	// fmt.Println(message)
	for ii := 0; ii < len(message); ii++ {

		if string(message[ii]) == "|" {
			arwServer.requestManager.StartOrStopUDPRequest(arwServer, conn)
		} else {
			arwServer.requestManager.AddUdpChar(string(message[ii]))
		}
	}
}

func (arwServer *ARWServer) AddEventHandler(event *ARWEvent, eventHandler EventHandler) {
	event.Handler = eventHandler
}

func (arwServer *ARWServer) AddExtensionHandler(cmd string, handler ExtensionHandler) error {
	for ii := 0; ii < len(arwServer.extensionHandlers); ii++ {
		currentExtension := arwServer.extensionHandlers[ii]
		if currentExtension.cmd == cmd {
			return errors.New("Extension Command already exist")
		}
	}

	var newExtension *ExtensionRequest
	newExtension = new(ExtensionRequest)

	newExtension.cmd = cmd
	newExtension.handler = handler
	arwServer.extensionHandlers = append(arwServer.extensionHandlers, newExtension)
	return nil
}

func (arwServer *ARWServer) SendExtensionRequest(cmd string, user *ARWUser, arwObj ARWObject, isTcp bool) {
	var request *Request
	request = new(Request)

	request.eventname = Extension_Response
	request.arwObject = arwObj
	request.specialParams.PutString("cmd", cmd)
	request.specialParams.PutString("isRoomReq", "false")

	if isTcp == true {
		arwServer.SendRequest(request, user.session.conn)
	} else {
		if user.session.udpAddr != nil {
			arwServer.SendUDPRequest(request, user.session.udpAddr)
		}
	}
}

func (arwServer *ARWServer) SendRequest(request *Request, conn net.Conn) {
	if conn == nil {
		return
	}
	var reqData string
	reqData = "|"
	reqData += string(request.Compress())
	reqData += "|"
	conn.Write([]byte(reqData))
	request = nil
}

func (arwServer *ARWServer) SendUDPRequest(request *Request, conn *net.UDPAddr) {
	if conn == nil {
		return
	}
	var reqData string
	reqData = "|" + string(request.Compress()) + "|"
	_, err := arwServer.udpListener.WriteToUDP([]byte(reqData), conn)
	if err != nil {
		fmt.Println("Send UDP Request Error ", conn)
		arwServer.logManager.NewSystemLog("Sending UDP Request Error : "+conn.String(), System_Error)
	}
}

func SendPostHttpRequest(url string, requestBody map[string]string) (string, error) {
	reqBody, err := json.Marshal(requestBody)
	if err != nil {
		return "", err
	}

	httpReq, _ := http.NewRequest("POST", url, bytes.NewBuffer(reqBody))
	httpReq.Header.Set("X-Custom-Header", "myvalue")
	httpReq.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	response, err := client.Do(httpReq)
	if err != nil {
		return "", err
	}
	defer response.Body.Close()
	respBody, _ := ioutil.ReadAll(response.Body)
	bodyString := string(respBody)

	return bodyString, nil
}

func SendGetHttpRequest(url string) (string, error) {

	httpReq, _ := http.NewRequest("GET", url, nil)
	httpReq.Header.Set("X-Custom-Header", "myvalue")
	httpReq.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	response, err := client.Do(httpReq)
	if err != nil {
		return "", err
	}
	defer response.Body.Close()
	respBody, _ := ioutil.ReadAll(response.Body)
	bodyString := string(respBody)

	return bodyString, nil
}

func bToMb(b uint64) uint64 {
	return b / 1024
}
