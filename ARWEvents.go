package main

import "net"

type EventHandler func(ARWObject)

type VoiceEventHandler func(ARWUser, VoiceObject)
type PrivateVoiceEventHandler func(*net.UDPAddr, map[string]interface{})

type ARWEvent struct {
	eventName           string
	Handler             EventHandler
	Private_Handler     func(*ARWServer, net.Conn, *Request)
	Private_Handler_UDP func(*ARWServer, *net.UDPAddr, *Request)
}

type ARWVoiceEvent struct {
	eventName string
	Handler   VoiceEventHandler
	P_Handler PrivateVoiceEventHandler
}

type ARWEvents struct {
	Connection            ARWEvent
	Disconnection         ARWEvent
	Login                 ARWEvent
	Room_Create           ARWEvent
	Join_Room             ARWEvent
	User_Enter_Room       ARWEvent
	User_Exit_Room        ARWEvent
	Extension_Request     ARWEvent
	UDP_Extension_Request ARWEvent
	Udp_Init              ARWEvent
	Udp_Init_Fail         ARWEvent
	Ping                  ARWEvent
	allEvents             []ARWEvent
	voiceEvents           *ARWVoiceEvents
}

type ARWVoiceEvents struct {
	Voice_Connection ARWVoiceEvent
	Voice_Message    ARWVoiceEvent
}

const (
	Exception_Error        = "EXCEPTION_ERROR"
	Connection_Success     = "CONNECTION_SUCCESS"
	Login                  = "LOGIN"
	Login_Error            = "LOGIN_ERROR"
	Room_Create            = "ROOM_CREATE"
	Join_Room              = "ROOM_JOIN"
	User_Enter_Room        = "USER_ENTER_ROOM"
	Disconnection          = "DISCONNECTION"
	User_Exit_Room         = "USER_EXIT_ROOM"
	Extension_Response     = "EXTENTION_REQUEST"
	UDP_Extension_Response = "UDP_EXTENTION_REQUEST"
	Udp_Init               = "UDP_INIT"
	Udp_Init_Fail          = "UDP_INIT_FAIL"
	Ping                   = "PING"
	Voice_Connection       = "V_Con"
	Voice_Connection_Error = "V_Con_Err"
	Voice_Message          = "V_Mes"
)

func (events *ARWEvents) Initialize() {
	events.voiceEvents = new(ARWVoiceEvents)
	events.Connection.eventName = Connection_Success
	events.Connection.Private_Handler = P_ConnectionSuccess

	events.Disconnection.eventName = Disconnection
	events.Disconnection.Private_Handler = P_Disconnection

	events.Login.eventName = Login
	events.Login.Private_Handler = P_Login

	events.Room_Create.eventName = Room_Create

	events.Join_Room.eventName = Join_Room

	events.User_Enter_Room.eventName = User_Enter_Room

	events.User_Exit_Room.eventName = User_Exit_Room

	events.Extension_Request.eventName = Extension_Response
	events.Extension_Request.Private_Handler = P_ExtensionResponse

	events.Udp_Init.eventName = Udp_Init
	events.Udp_Init.Private_Handler_UDP = P_Udp_Init

	events.Udp_Init_Fail.eventName = Udp_Init_Fail
	events.Udp_Init_Fail.Private_Handler_UDP = P_Udp_Init_Fail

	events.UDP_Extension_Request.eventName = UDP_Extension_Response
	events.UDP_Extension_Request.Private_Handler_UDP = P_ExtensionResponse_UDP

	events.Ping.eventName = Ping
	events.Ping.Private_Handler = P_Ping

	events.allEvents = make([]ARWEvent, 0, 15)
	events.allEvents = append(events.allEvents, events.Connection)
	events.allEvents = append(events.allEvents, events.Disconnection)
	events.allEvents = append(events.allEvents, events.Login)
	events.allEvents = append(events.allEvents, events.Room_Create)
	events.allEvents = append(events.allEvents, events.Join_Room)
	events.allEvents = append(events.allEvents, events.User_Enter_Room)
	events.allEvents = append(events.allEvents, events.User_Exit_Room)
	events.allEvents = append(events.allEvents, events.Extension_Request)
	events.allEvents = append(events.allEvents, events.Udp_Init)
	events.allEvents = append(events.allEvents, events.UDP_Extension_Request)
	events.allEvents = append(events.allEvents, events.Ping)
	//=============================================

	events.voiceEvents.Voice_Connection.eventName = Voice_Connection
	events.voiceEvents.Voice_Connection.P_Handler = P_Voice_Connection

	events.voiceEvents.Voice_Message.eventName = Voice_Message
	events.voiceEvents.Voice_Message.P_Handler = P_Voice_Message
}
