package main

import (
	"errors"
	"strconv"
	"strings"
)

type RoomInitializeMethod func(arwServer *ARWServer, room *ARWRoom)
type RoomEvent func(*ARWServer, *ARWUser, *ARWRoom)

type ARWRoom struct {
	tag               string
	name              string
	status            string
	id                int
	cappacity         int
	userList          []*ARWUser
	extensionRequests []*ExtensionRequest
	InitializeMethod  RoomInitializeMethod
	admin             *ARWUser
	userEnterRoom     RoomEvent
	userExitRoom      RoomEvent
	healthObjects     map[int]bool
}

func (room *ARWRoom) Init(arwServer *ARWServer) {
	arwServer.logManager.NewSystemLog("Room Created : "+room.name+" : "+room.tag, System_Log)
	room.userList = make([]*ARWUser, 0, room.cappacity)
	room.extensionRequests = make([]*ExtensionRequest, 0, arwServer.serverSettings.maxRoomExtensionRequests)

	if room.InitializeMethod != nil {
		room.InitializeMethod(arwServer, room)
	}
}

func (room *ARWRoom) AddUserInRoom(user *ARWUser, arwServer *ARWServer) error {

	if len(room.userList) >= room.cappacity {
		arwServer.logManager.NewSystemLog("Room cappacity is full - Room Name : "+room.name, System_Error)
		return errors.New("Room is full")
	}

	if user.lastRoom != nil && user.lastRoom == room {
		arwServer.logManager.NewSystemLog("User already joined this room - Room Name : "+room.name+" - User ID : "+user.name, System_Error)
		return errors.New("User already joined this room")
	}

	if len(room.userList) == 0 {
		room.admin = user
	}

	for ii := 0; ii < len(room.userList); ii++ {
		var userEnterRoomRequest *Request
		userEnterRoomRequest = new(Request)
		userEnterRoomRequest.eventname = User_Enter_Room

		userEnterRoomRequest.specialParams.PutString("user_properties", user.CompressUserProperties(room.userList[ii]))
		userEnterRoomRequest.specialParams.PutInt("room_id", room.id)
		arwServer.SendRequest(userEnterRoomRequest, room.userList[ii].session.conn)
		userEnterRoomRequest = nil
	}

	room.userList = append(room.userList, user)
	user.lastRoom = room

	var roomJoinRequest *Request
	roomJoinRequest = new(Request)

	roomJoinRequest.eventname = Join_Room
	roomJoinRequest.specialParams.PutString("room_properties", room.CompressRoomSettings(user))
	arwServer.SendRequest(roomJoinRequest, user.session.conn)

	if room.userEnterRoom != nil {
		room.userEnterRoom(arwServer, user, room)
	}

	return nil
}

func (room *ARWRoom) RemoveUserInRoom(user *ARWUser, arwServer *ARWServer) {
	for ii := 0; ii < len(room.userList); ii++ {
		if room.userList[ii].id == user.id {
			arwServer.logManager.NewSystemLog("User exit room - room name : "+room.name+" - user id : "+user.name, System_Log)
			room.userList = append(room.userList[:ii], room.userList[ii+1:]...)

			if room.userExitRoom != nil {
				room.userExitRoom(arwServer, user, room)
			}

			var userExitRoomRequest *Request
			userExitRoomRequest = new(Request)

			userExitRoomRequest.eventname = User_Exit_Room
			userExitRoomRequest.specialParams.PutInt("user_id", user.id)
			userExitRoomRequest.specialParams.PutInt("room_id", room.id)
			for ii := 0; ii < len(room.userList); ii++ {
				arwServer.SendRequest(userExitRoomRequest, room.userList[ii].session.conn)
			}
			userExitRoomRequest = nil
			if len(room.userList) <= 0 {
				arwServer.roomManager.DestroyRoom(room)
			}
		}
	}
}

func (room *ARWRoom) AddExtensionHandler(cmd string, handler ExtensionRoomHandler) error {
	for _, extension := range room.extensionRequests {
		if extension.cmd == cmd {
			return errors.New("Extension Already Exist")
		}
	}

	var newExtension *ExtensionRequest
	newExtension = new(ExtensionRequest)

	newExtension.cmd = cmd
	newExtension.roomHandler = handler

	room.extensionRequests = append(room.extensionRequests, newExtension)
	return nil
}

func (room *ARWRoom) SendExtensionRequest(arwServer *ARWServer, cmd string, arwObj ARWObject, user *ARWUser, isTcp bool) {
	var req *Request
	req = new(Request)

	req.eventname = Extension_Response
	req.arwObject = arwObj
	req.specialParams.PutString("cmd", cmd)
	req.specialParams.PutString("isRoom", "true")
	req.specialParams.PutInt("roomId", room.id)

	if isTcp == true {
		arwServer.SendRequest(req, user.session.conn)
	} else {
		if user.session.udpAddr != nil {
			arwServer.SendUDPRequest(req, user.session.udpAddr)
		}
	}
}

func (room *ARWRoom) CompressRoomSettings(user *ARWUser) string {

	roomData := "{"
	roomData += "\"name\":\"" + room.name + "\","
	roomData += "\"tag\":\"" + room.tag + "\","
	roomData += "\"id\":" + strconv.Itoa(room.id) + ","
	roomData += "\"users\":["

	for ii := 0; ii < len(room.userList); ii++ {
		roomData += room.userList[ii].CompressUserProperties(user) + ","
	}

	roomData = strings.TrimRight(roomData, ",")
	roomData += "]}"
	return roomData
}

func (room *ARWRoom) IsFull() bool {
	if len(room.userList) >= room.cappacity {
		return true
	}
	return false
}

func (room *ARWRoom) SendExtensionRequestToAllUsers(arwServer *ARWServer, cmd string, arwObj ARWObject, isTcp bool) {

	for _, user := range room.userList {
		room.SendExtensionRequest(arwServer, cmd, arwObj, user, isTcp)
	}
}

func (room *ARWRoom) SendExtensionRequestWithuserMask(arwServer *ARWServer, cmd string, arwObj ARWObject, userMask *ARWUser, isTcp bool) {
	for _, user := range room.userList {
		if user.id != userMask.id {
			room.SendExtensionRequest(arwServer, cmd, arwObj, user, isTcp)
		}
	}
}

func (room *ARWRoom) FindUserById(userId int) *ARWUser {
	for _, user := range room.userList {
		if user.id == userId {
			return user
		}
	}

	return nil
}
