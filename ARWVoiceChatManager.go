package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net"
)

type VoiceObject struct {
	data []byte
}

type ARWVoiceChatManager struct {
	port           int
	voiceListener  *net.UDPConn
	wrongVoiceData string
	beginVoiceReq  bool
}

func (voiceManager *ARWVoiceChatManager) Init(port int) {
	voiceManager.port = port
	voiceAddr, err := net.ResolveUDPAddr("udp", ":10004")
	if err != nil {
		panic(err)
	}

	voiceListener, err := net.ListenUDP("udp", voiceAddr)
	if err != nil {
		panic(err)
	}
	voiceManager.voiceListener = voiceListener
	fmt.Println("ARWServer VoiceChat Initialize Success!!")
	arwServer.logManager.NewSystemLog("VoiceChat Initialize Success", System_Log)
}

func (voiceManager *ARWVoiceChatManager) ProcessEvent() {
	if voiceManager.voiceListener == nil {
		return
	}

	buf := make([]byte, 25000)
	_, udpAddr, err := voiceManager.voiceListener.ReadFromUDP(buf)
	if err != nil {
		fmt.Println("Read VoiceChat Error ", err)
	} else {
		buf = bytes.Trim(buf, "\x00")
		go voiceManager.ParseVoiceMessages(buf, udpAddr)
	}
	buf = nil
}

func (voiceManger *ARWVoiceChatManager) ParseVoiceMessages(bytes []byte, conn *net.UDPAddr) {

	message := string(bytes)
	for ii := 0; ii < len(message); ii++ {
		if string(message[ii]) == "|" {
			arwServer.voiceManager.StartOrStopVoiceRequest(conn)
		} else {
			arwServer.voiceManager.AddVoiceChar(string(message[ii]))
		}
	}
}

func (voiceManager *ARWVoiceChatManager) StartOrStopVoiceRequest(conn *net.UDPAddr) {
	if voiceManager.beginVoiceReq == false {
		voiceManager.beginVoiceReq = true
		return
	}

	// fmt.Println(voiceManager.wrongVoiceData)
	voiceData := make(map[string]interface{})

	err := json.Unmarshal([]byte(voiceManager.wrongVoiceData), &voiceData)
	if err != nil {
		return
	}
	voiceManager.DoVoiceRequest(voiceData, conn)
	voiceManager.wrongVoiceData = ""
	voiceManager.beginVoiceReq = false
}

func (voiceManager *ARWVoiceChatManager) AddVoiceChar(reqChar string) {
	voiceManager.wrongVoiceData += reqChar
}

func (voiceManager *ARWVoiceChatManager) DoVoiceRequest(request map[string]interface{}, conn *net.UDPAddr) {
	eventName := request["event_name"].(string)

	switch eventName {
	case Voice_Connection:
		if arwServer.events.voiceEvents.Voice_Connection.P_Handler == nil {
			break
		}
		arwServer.events.voiceEvents.Voice_Connection.P_Handler(conn, request)
		break
	case Voice_Message:
		if arwServer.events.voiceEvents.Voice_Message.P_Handler == nil {
			break
		}
		arwServer.events.voiceEvents.Voice_Message.P_Handler(conn, request)
		break
	}
}

func (voiceManager *ARWVoiceChatManager) SendVoiceEvent(voicePort *net.UDPAddr, data string) {
	if voicePort == nil {
		return
	}

	_, err := arwServer.voiceManager.voiceListener.WriteToUDP([]byte(data), voicePort)
	if err != nil {
		fmt.Println("Voice Message Error ", err)
	}
}
