package main

import (
	"fmt"
	"net"
	"strconv"
)

func P_ConnectionSuccess(arwServer *ARWServer, conn net.Conn, request *Request) {

	ses := arwServer.sessionManager.StartSession(conn)

	var connectionRequest *Request
	connectionRequest = new(Request)

	connectionRequest.eventname = Connection_Success
	connectionRequest.specialParams.PutString("error", "")
	connectionRequest.specialParams.PutInt("session_id", int(ses.id))

	arwServer.SendRequest(connectionRequest, conn)
	connectionRequest = nil
	if arwServer.events.Connection.Handler == nil {
		return
	}
	go arwServer.events.Connection.Handler(request.arwObject)
}

func P_Disconnection(arwServer *ARWServer, conn net.Conn, request *Request) {
	arwServer.sessionManager.CloseSession(arwServer, conn)

	if arwServer.events.Disconnection.Handler == nil {
		return
	}

	go arwServer.events.Disconnection.Handler(request.arwObject)
}

func P_Login(arwServer *ARWServer, conn net.Conn, request *Request) {

	userName, _ := request.specialParams.GetString("user_name")
	user, err := arwServer.userManager.CreateUser(arwServer, conn, userName)

	if err != nil {
		fmt.Println("User Create Error :", err)
		arwServer.logManager.NewSystemLog("User Login Error : "+err.Error(), System_Error)
		loginFailReq := new(Request)
		loginFailReq.eventname = Login_Error
		loginFailReq.arwObject.PutString("error", err.Error())
		arwServer.SendRequest(loginFailReq, conn)
		return
	}

	arwServer.logManager.NewSystemLog("User Login Success : User Name : "+user.name, System_Log)
	var responseRequest *Request
	responseRequest = new(Request)

	responseRequest.eventname = Login
	responseRequest.specialParams.PutString("user_properties", user.CompressUserProperties(user))

	arwServer.SendRequest(responseRequest, conn)
	responseRequest = nil

	if arwServer.events.Login.Handler == nil {
		return
	}

	request.arwObject.PutInt("user_id", user.id)
	go arwServer.events.Login.Handler(request.arwObject)
}

func P_ExtensionResponse(arwServer *ARWServer, conn net.Conn, request *Request) {

	cmd, _ := request.specialParams.GetString("cmd")
	isRoomReq, _ := request.specialParams.GetString("isRoom")

	if isRoomReq == "False" {
		for _, extension := range arwServer.extensionHandlers {
			if cmd == extension.cmd {
				user, err := arwServer.userManager.FindUserWithSession(conn)
				if err == nil {
					go extension.handler(arwServer, user, request.arwObject)
					return
				}
			}
		}
	} else {
		roomId, err := request.specialParams.GetInt("room_id")
		if err != nil {
			return
		}

		room := arwServer.roomManager.FindRoomWithRoomId(roomId)
		if room == nil {
			return
		}
		for _, extension := range room.extensionRequests {
			if extension.cmd == cmd {
				user, userErr := arwServer.userManager.FindUserWithSession(conn)
				if userErr != nil || user == nil {
					return
				}

				go extension.roomHandler(arwServer, user, request.arwObject, room)
			}
		}
	}
}

func P_Udp_Init(arwServer *ARWServer, addr *net.UDPAddr, request *Request) {

	session_id, err := request.specialParams.GetInt("session_id")
	if err != nil {
		udpInitFailReq := new(Request)
		udpInitFailReq.eventname = Udp_Init_Fail

		var obj ARWObject
		obj.PutString("error", "UDP Init Session ID Invalid Type")
		arwServer.SendUDPRequest(udpInitFailReq, addr)
		udpInitFailReq = nil

		arwServer.logManager.NewSystemLog("UDP Init Failed : Session ID does not exist", System_Error)
		return
	}

	currentSession := arwServer.sessionManager.FindSessionIdByID(uint32(session_id))
	if currentSession == nil {
		var udpInitFailReq Request
		udpInitFailReq.eventname = Udp_Init_Fail

		var obj ARWObject
		obj.PutString("error", "There is no session")
		arwServer.SendUDPRequest(&udpInitFailReq, addr)

		arwServer.logManager.NewSystemLog("UDP Init Failed : Session does not exist", System_Error)
		return
	}

	var req *Request
	req = new(Request)
	req.eventname = Udp_Init
	arwServer.SendRequest(req, currentSession.conn)
	currentSession.SetUdpAddr(addr)
	fmt.Println("UDP Init Success ", currentSession.udpAddr)
	req = nil

	arwServer.logManager.NewSystemLog("UDP Init Success : "+currentSession.udpAddr.String(), System_Log)
}

func P_Udp_Init_Fail(arwServer *ARWServer, addr *net.UDPAddr, request *Request) {

}

func P_ExtensionResponse_UDP(arwServer *ARWServer, addr *net.UDPAddr, request *Request) {
	cmd, _ := request.specialParams.GetString("cmd")
	isRoomReq, _ := request.specialParams.GetString("isRoom")

	user, err := arwServer.userManager.FindUserWithUDPSession(addr)
	if err != nil {
		return
	}

	if isRoomReq == "False" {
		for _, extension := range arwServer.extensionHandlers {
			if cmd == extension.cmd {
				go extension.handler(arwServer, user, request.arwObject)
				return
			}
		}
	} else {
		if user.lastRoom == nil || len(user.lastRoom.extensionRequests) == 0 {
			return
		}
		for _, extension := range user.lastRoom.extensionRequests {
			if extension.cmd == cmd {
				go extension.roomHandler(arwServer, user, request.arwObject, user.lastRoom)
				return
			}
		}
	}
}

func P_Voice_Connection(conn *net.UDPAddr, packageData map[string]interface{}) {
	session_id := packageData["ses_id"].(string)
	ses_int, err := strconv.Atoi(session_id)
	if err != nil {
		fmt.Println("Voice Chat Init Error ", err)
		responseData := "|{"
		responseData += "\"event_name\":\"" + Voice_Connection_Error + "\","
		responseData += "\"error\":\"" + err.Error() + "\""
		responseData += "}|"
		arwServer.voiceManager.SendVoiceEvent(conn, responseData)
		return
	}
	currentSession := arwServer.sessionManager.FindSessionIdByID(uint32(ses_int))
	if currentSession == nil {
		fmt.Println("Voice Connection Failed")
		responseData := "|{"
		responseData += "\"event_name\":\"" + Voice_Connection_Error + "\","
		responseData += "\"error\":\"" + err.Error() + "\""
		responseData += "}|"
		arwServer.voiceManager.SendVoiceEvent(conn, responseData)
		return
	}
	currentSession.voiceaddr = conn
	responseData := "|{"
	responseData += "\"event_name\":\"" + Voice_Connection + "\""
	responseData += "}|"

	arwServer.voiceManager.SendVoiceEvent(currentSession.voiceaddr, responseData)

	fmt.Println("Voice Chat Client Initialize Success ", session_id)
}

func P_Voice_Message(conn *net.UDPAddr, packageData map[string]interface{}) {
	sessionIsExist := arwServer.sessionManager.SessionIsExitsByVoicePort(conn)
	if sessionIsExist == false {
		return
	}

	channels := strconv.FormatFloat(packageData["channels"].(float64), 'f', -1, 64)
	u_id := strconv.FormatFloat(packageData["u_id"].(float64), 'f', -1, 64)

	responseData := "|{"
	responseData += "\"event_name\":\"" + Voice_Message + "\","
	responseData += "\"channels\": " + channels + ","
	responseData += "\"u_id\": " + u_id + ","
	responseData += "\"data\": " + "\"" + packageData["data"].(string) + "\""
	responseData += "}|"
	arwServer.voiceManager.SendVoiceEvent(conn, responseData)
}

func P_Ping(arwServer *ARWServer, conn net.Conn, request *Request) {
	arwServer.SendRequest(request, conn)
}
