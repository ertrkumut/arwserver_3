package main

import (
	"fmt"
	"net"
)

type Session struct {
	conn      net.Conn
	udpAddr   *net.UDPAddr
	voiceaddr *net.UDPAddr
	id        uint32
}

func (s *Session) Init(conn net.Conn, sessionManager *SessionManager) {
	sessionManager.sessionIdCounter++
	s.conn = conn

	s.id = sessionManager.sessionIdCounter

	sessionManager.allSessions = append(sessionManager.allSessions, s)
	arwServer.logManager.NewSystemLog("Session Started : "+string(s.id), System_Log)
	fmt.Println("Session Started : ", s.id)
}

func (s *Session) SetUdpAddr(udpAddr *net.UDPAddr) {
	s.udpAddr = udpAddr
}
