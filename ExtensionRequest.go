package main

type ExtensionHandler func(*ARWServer, *ARWUser, ARWObject)
type ExtensionRoomHandler func(*ARWServer, *ARWUser, ARWObject, *ARWRoom)

type ExtensionRequest struct {
	cmd         string
	handler     ExtensionHandler
	roomHandler ExtensionRoomHandler
}
