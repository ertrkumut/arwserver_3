package main

import (
	"fmt"
	"os"
	"time"
)

type LogManager struct {
	logFile string
}

func (logManager *LogManager) Init(logFilePath string) {
	logManager.logFile = logFilePath

	_, err := os.Create(logFilePath)

	if err != nil {
		panic(err)
	}
}

func (logManager *LogManager) NewSystemLog(msg string, _type string) {
	fileHandler, err := os.OpenFile(arwServer.logManager.logFile, os.O_WRONLY|os.O_APPEND, 0644)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer fileHandler.Close()

	_time := time.Now().Format(time.StampMilli)

	logData := _type + "|" + msg + "|" + _time + "\n"
	fileHandler.WriteString(logData)
}

func newLog(message string) {

	fileHandler, err := os.OpenFile(arwServer.logManager.logFile, os.O_WRONLY|os.O_APPEND, 0644)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer fileHandler.Close()

	_type := "USER_LOG"
	_time := time.Now().Format(time.StampMilli)

	logData := _type + "|" + message + "|" + _time + "\n"
	fileHandler.WriteString(logData)
}
